package edu.mit.ezyang.gravity.actors;

import com.sun.j3d.utils.geometry.Sphere;
import javax.vecmath.Vector3f;

/**
 *
 * @author Edward Z. Yang <ezyang@mit.edu>
 */
public class Body extends Sphere {
    public float mass = 0;
    public Vector3f velocity = new Vector3f(0f, 0f, 0f);
    public Body() {
    }
    public Body(float scale) {
        super(scale);
    }
}
