/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mit.ezyang.gravity.j3d;

import com.sun.j3d.utils.geometry.Cylinder;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;

/**
 *
 * @author Edward Z. Yang <ezyang@mit.edu>
 */
public class ArrowGroup extends BranchGroup {
    public ArrowGroup(Vector3f velocity) {
        Cylinder stem = new Cylinder(0.02f, velocity.length());
        stem.getAppearance().getMaterial().setEmissiveColor(0.3f, 0.3f, 0.8f);
        
        setCapability(BranchGroup.ALLOW_DETACH);
        TransformGroup arrowTransformGroup = new TransformGroup();
        Transform3D arrowTransform3D = new Transform3D();

        Vector3f translation = new Vector3f();
        translation.scale(1f/2f, velocity);

        Vector3f vec_y = (Vector3f) velocity.clone();
        vec_y.normalize();

        Vector3f vec_x; // reference vector, will correct later
        if (vec_y.x == 0 && vec_y.z == 0) {
            vec_x = new Vector3f(-vec_y.y, 0f, 0f); // could be optimized
        } else {
            vec_x = new Vector3f(0f, 1f, 0f);
        }

        Vector3f vec_z = new Vector3f();
        vec_z.cross(vec_x, vec_y);
        vec_z.normalize();

        vec_x.cross(vec_z, vec_y);
        vec_x.normalize();
        vec_x.negate();

        Matrix3f rotation = new Matrix3f(
            vec_x.x, vec_x.y, vec_x.z,
            vec_y.x, vec_y.y, vec_y.z,
            vec_z.x, vec_z.y, vec_z.z
        );
        rotation.invert();

        arrowTransform3D.set(rotation, translation, 1f);

        arrowTransformGroup.setTransform(arrowTransform3D);
        arrowTransformGroup.addChild(stem);
        addChild(arrowTransformGroup);
    }
}
