package edu.mit.ezyang.gravity.j3d.behaviors;

import edu.mit.ezyang.gravity.GravitySimulatorView;
import edu.mit.ezyang.gravity.actors.Body;
import edu.mit.ezyang.gravity.j3d.EditableGroup;
import java.util.Enumeration;
import javax.media.j3d.Behavior;
import javax.media.j3d.Node;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.WakeupOnElapsedTime;
import javax.vecmath.Vector3f;

/**
 *
 * @author Edward Z. Yang <ezyang@mit.edu>
 */
public class PhysicsBehavior extends Behavior {
    protected TransformGroup targetTG;
    protected GravitySimulatorView view;
    protected int interval = 10;
    static public float G = .01f;
    public PhysicsBehavior(TransformGroup tg, GravitySimulatorView view) {
        this.targetTG = tg;
        // Yeah, I know this is very hacky. We need to separate out the
        // rendering components from the GUI view class
        this.view = view;
    }
    public void initialize() {
        this.wakeupOn(new WakeupOnElapsedTime(interval));
    }
    public void processStimulus(Enumeration criteria) {
        if (targetTG.getChild(0) instanceof Body) {
            Body body = (Body) targetTG.getChild(0);
            Transform3D transform = new Transform3D();
            targetTG.getTransform(transform);
            Vector3f trans = new Vector3f();
            transform.get(trans);
            // velocity modifications from force
            if (body.mass != 0f) {
                Enumeration<Node> children = view.universeRoot.getAllChildren();
                while (children.hasMoreElements()) {
                    Node node = children.nextElement();
                    if (node instanceof EditableGroup) {
                        TransformGroup group = ((EditableGroup) node).getTransformGroup();
                        Node subnode = group.getChild(0);
                        if (subnode instanceof Body) {
                            // ok, this can affect our dude
                            Body otherBody = (Body) subnode;
                            if (otherBody == body) continue;
                            // disregard massless objects
                            if (otherBody.mass == 0) continue;
                            Transform3D otherTransform = new Transform3D();
                            group.getTransform(otherTransform);
                            Vector3f otherVector = new Vector3f();
                            otherTransform.get(otherVector);
                            otherVector.sub(trans);
                            float distance = Math.abs(otherVector.length());
                            if (distance == 0f) continue;
                            float force = G * otherBody.mass * body.mass / (distance * distance);
                            float accel = force / body.mass;
                            otherVector.normalize();
                            otherVector.scale(accel);
                            body.velocity.add(otherVector);
                            if (body == view.selectedPrimitive) {
                                view.notifyPick(body);
                            }
                        }
                    }
                }
            }
            
            // now redraw the object
            if (body.velocity.length() != 0f) {
                trans.x += body.velocity.x / 1000 * interval;
                trans.y += body.velocity.y / 1000 * interval;
                trans.z += body.velocity.z / 1000 * interval;
                transform.set(trans, 1.0f);
                targetTG.setTransform(transform);
                postId(1);
            }
        }
        this.wakeupOn(new WakeupOnElapsedTime(interval));
    }
}
