/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mit.ezyang.gravity.j3d;

import edu.mit.ezyang.gravity.GravitySimulatorView;
import edu.mit.ezyang.gravity.j3d.behaviors.PhysicsBehavior;
import edu.mit.ezyang.gravity.j3d.utils.picking.behaviors.PickSelectBehavior;
import edu.mit.ezyang.gravity.j3d.utils.picking.behaviors.SmartPickTranslateBehavior;
import edu.mit.ezyang.gravity.j3d.utils.picking.behaviors.SmartPickZoomBehavior;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.Bounds;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.Node;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;

/**
 * Convenience group for adding elements that need to be editable.
 * @warning Actual object should be added using addTransformChild(), NOT 
 *          addChild().
 * @author Edward Z. Yang <ezyang@mit.edu>
 */
public class EditableGroup extends BranchGroup {

    private TransformGroup transformGroup;
    
    public EditableGroup(Canvas3D canvas, TransformGroup rotateGroup, Vector3f location, GravitySimulatorView callback) {
        setCapability(BranchGroup.ALLOW_DETACH);

        Bounds bounds = new BoundingSphere(new Point3d(0f, 0f, 0f), 1000f);

        SmartPickTranslateBehavior translate = new SmartPickTranslateBehavior(this, canvas, bounds);
        translate.setRotateTransformGroup(rotateGroup);
        addChild(translate);

        // :TODO: this needs to be modified to be smart
        SmartPickZoomBehavior zoom = new SmartPickZoomBehavior(this, canvas, bounds);
        zoom.setRotateTransformGroup(rotateGroup);
        addChild(zoom);
        
        PickSelectBehavior select = new PickSelectBehavior(this, canvas, bounds);
        select.setupCallback(callback);
        addChild(select);
        
        transformGroup = new TransformGroup();
        transformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        transformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        transformGroup.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
        transformGroup.setCapability(TransformGroup.ALLOW_CHILDREN_EXTEND);
        transformGroup.setCapability(TransformGroup.ALLOW_CHILDREN_WRITE);
        transformGroup.setCapability(TransformGroup.ALLOW_CHILDREN_READ);

        Transform3D transform = new Transform3D();
        transform.setTranslation(location);
        transformGroup.setTransform(transform);
        
        PhysicsBehavior physics = new PhysicsBehavior(transformGroup, callback);
        physics.setSchedulingBounds(bounds);
        addChild(physics);
        
        addChild(transformGroup);
    }
    
    public TransformGroup getTransformGroup() {
        return transformGroup;
    }
    
    public void addTransformChild(Node child) {
        transformGroup.addChild(child);
    }
    
    
}
