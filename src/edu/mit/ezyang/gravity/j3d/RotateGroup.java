package edu.mit.ezyang.gravity.j3d;

import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import javax.media.j3d.*;
import javax.vecmath.*;

/**
 * Customized TransformGroup class that implements mouse rotations, directional
 * lighting and ambient lighting; this is the "frame" in which the universe
 * operates, and can be rotated at the user's viewing pleasure.
 * @author Edward Z. Yang <ezyang@mit.edu>
 */
public class RotateGroup extends TransformGroup {
    public RotateGroup() {
        setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
	setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        
        BoundingSphere bounds = new BoundingSphere(new Point3d(0f,0f,0f), 1000f);
        
        MouseRotate rotate = new MouseRotate();
        rotate.setSchedulingBounds(bounds);
        rotate.setTransformGroup(this);
        addChild(rotate);
        
        DirectionalLight light = new DirectionalLight(new Color3f(1f, 1f, 1f), new Vector3f(4.0f, -7.0f, 7.0f));
        light.setInfluencingBounds(bounds);
        addChild(light);
        
        AmbientLight ambLight = new AmbientLight(new Color3f(.6f, .6f, .6f));
        ambLight.setInfluencingBounds(bounds);
        addChild(ambLight);
    }
}
