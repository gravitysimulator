/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.mit.ezyang.gravity.j3d.utils.picking.behaviors;

import edu.mit.ezyang.gravity.j3d.utils.picking.behaviors.PickSelectCallback;
import com.sun.j3d.utils.behaviors.mouse.MouseBehaviorCallback;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.picking.PickResult;
import com.sun.j3d.utils.picking.behaviors.PickMouseBehavior;
import java.awt.event.MouseEvent;
import javax.media.j3d.Bounds;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.Transform3D;

/**
 *
 * @author Edward Z. Yang <ezyang@mit.edu>
 */
public class PickSelectBehavior extends PickMouseBehavior implements MouseBehaviorCallback {
    protected PickSelectCallback callback;
    public PickSelectBehavior(BranchGroup root, Canvas3D canvas, Bounds bounds) {
        super(canvas, root, bounds);
        setSchedulingBounds(bounds);
    }
    public void setupCallback(PickSelectCallback callback) {
        this.callback = callback;
    }
    public void updateScene(int xpos, int ypos) {
        Primitive primitive = null;
        if (mevent.getButton() == MouseEvent.BUTTON1) {
            pickCanvas.setShapeLocation(xpos, ypos);
            PickResult pr = pickCanvas.pickClosest();
            if ((pr != null) &&
                    ((primitive = (Primitive) pr.getNode(PickResult.PRIMITIVE)) != null) &&
                    (primitive.getCapability(Primitive.ENABLE_APPEARANCE_MODIFY ))) {
                if (callback != null) {
                    callback.notifyPick(primitive);
                }
            }
        }
    }

    /**
     * Callback method from MouseTranslate
     * This is used when the Picking callback is enabled
     */
    public void transformChanged(int type, Transform3D transform) {
    }
}
