/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.mit.ezyang.gravity.j3d.utils.picking.behaviors;

import com.sun.j3d.utils.geometry.Primitive;

/**
 *
 * @author Edward Z. Yang <ezyang@mit.edu>
 */
public interface PickSelectCallback {
    public void notifyPick(Primitive node);
}
