/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mit.ezyang.gravity.j3d.utils.picking.behaviors;

import edu.mit.ezyang.gravity.j3d.utils.behaviors.mouse.SmartMouseZoom;
import edu.mit.ezyang.gravity.j3d.*;
import com.sun.j3d.utils.behaviors.mouse.MouseBehavior;
import com.sun.j3d.utils.behaviors.mouse.MouseBehaviorCallback;
import com.sun.j3d.utils.behaviors.mouse.MouseZoom;
import com.sun.j3d.utils.picking.PickResult;
import com.sun.j3d.utils.picking.behaviors.PickMouseBehavior;
import com.sun.j3d.utils.picking.behaviors.PickingCallback;
import javax.media.j3d.Bounds;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;

/**
 * Reimplemented PickZoomBehavior, but uses SmartMouseWheelZoom, which
 * has been customized to take into account rotations relative to the surface
 * display. Modifications are solely in constructor and new setRotateTransformGroup
 * method.
 * @author Edward Z. Yang <ezyang@mit.edu>
 */
public class SmartPickZoomBehavior extends PickMouseBehavior implements MouseBehaviorCallback {

    private SmartMouseZoom zoom;
    private PickingCallback callback = null;
    private TransformGroup currentTG;

    /**
     * Propagates rotation transform group to SmartMouseTranslate in order
     * to provide appropriate information to transform.
     * @note We only support one rotation transform group; this should be
     *       enough for people with shallow object graphs.
     * @param group TransformGroup with rotation we need to adjust to
     */
    public void setRotateTransformGroup(TransformGroup group) {
        zoom.setRotateTransformGroup(group);
    }

    /**
     * Creates a pick/zoom behavior that waits for user mouse events for
     * the scene graph. 
     * @param root   Root of your scene graph.
     * @param canvas Java 3D drawing canvas.
     * @param bounds Bounds of your scene.
     **/
    public SmartPickZoomBehavior(BranchGroup root, Canvas3D canvas, Bounds bounds) {
        super(canvas, root, bounds);
        zoom = new SmartMouseZoom(MouseBehavior.MANUAL_WAKEUP);
        zoom.setTransformGroup(currGrp);
        currGrp.addChild(zoom);
        zoom.setSchedulingBounds(bounds);
        this.setSchedulingBounds(bounds);
    }

    /**
     * Update the scene to manipulate any nodes. This is not meant to be 
     * called by users. Behavior automatically calls this. You can call 
     * this only if you know what you are doing.
     * 
     * @param xpos Current mouse X pos.
     * @param ypos Current mouse Y pos.
     **/
    public void updateScene(int xpos, int ypos) {
        TransformGroup tg = null;

        if (mevent.isAltDown() && !mevent.isMetaDown()) {

            pickCanvas.setShapeLocation(xpos, ypos);
            PickResult pr = pickCanvas.pickClosest();
            if ((pr != null) &&
                    ((tg = (TransformGroup) pr.getNode(PickResult.TRANSFORM_GROUP)) != null) &&
                    (tg.getCapability(TransformGroup.ALLOW_TRANSFORM_READ)) &&
                    (tg.getCapability(TransformGroup.ALLOW_TRANSFORM_WRITE))) {
                zoom.setTransformGroup(tg);
                zoom.wakeup();
                currentTG = tg;
            // Need to clean up Issue 123 --- Chien        
            // freePickResult(pr);
            } else if (callback != null) {
                callback.transformChanged(PickingCallback.NO_PICK, null);
            }
        }
    }

    /**
     * Callback method from MouseZoom
     * This is used when the Picking callback is enabled
     */
    public void transformChanged(int type, Transform3D transform) {
        callback.transformChanged(PickingCallback.ZOOM, currentTG);
    }

    /**
     * Register the class @param callback to be called each
     * time the picked object moves
     */
    public void setupCallback(PickingCallback callback) {
        this.callback = callback;
        if (callback == null) {
            zoom.setupCallback(null);
        } else {
            zoom.setupCallback(this);
        }
    }
}
