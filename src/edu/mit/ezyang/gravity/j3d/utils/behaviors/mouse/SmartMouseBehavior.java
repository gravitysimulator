/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.mit.ezyang.gravity.j3d.utils.behaviors.mouse;

import javax.media.j3d.TransformGroup;

/**
 * Common methods to "smart" mouse behaviors which account for parent group
 * rotations.
 * @author Edward Z. Yang <ezyang@mit.edu>
 */
public interface SmartMouseBehavior {
    abstract public void setRotateTransformGroup(TransformGroup group);
}
