/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.mit.ezyang.gravity.j3d.utils.behaviors.mouse;

import com.sun.j3d.utils.behaviors.mouse.MouseZoom;
import java.awt.AWTEvent;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.WakeupCriterion;
import javax.media.j3d.WakeupOnAWTEvent;
import javax.media.j3d.WakeupOnBehaviorPost;
import javax.vecmath.Vector3d;

/**
 * Modified MouseWheelZoom class which accounts for a rotation in a parent
 * TransformGroup when calculating translation based off of screen change.
 * @warning Only the flags constructor was reimplemented.
 * @author Edward Z. Yang <ezyang@mit.edu>
 */
public class SmartMouseZoom extends MouseZoom implements SmartMouseBehavior {

    protected TransformGroup rotateTG;

    public SmartMouseZoom(int flags) {
        super(flags);
    }

    /**
     * Sets the TransformGroup used to determine the rotation that will
     * be adjusted against.
     * @param group TransformGroup with Transform3D rotation to adjust for
     */
    public void setRotateTransformGroup(TransformGroup group) {
        rotateTG = group;
    }

    /**
     * @note Overridden in order to force MouseTranslate to call our overloaded
     *       doProcess() function.
     * @param criteria
     */
    @Override
    public void processStimulus(Enumeration criteria) {
        WakeupCriterion wakeup;
        AWTEvent[] events;
        MouseEvent evt;
// 	int id;
// 	int dx, dy;

        while (criteria.hasMoreElements()) {
            wakeup = (WakeupCriterion) criteria.nextElement();
            if (wakeup instanceof WakeupOnAWTEvent) {
                events = ((WakeupOnAWTEvent) wakeup).getAWTEvent();
                if (events.length > 0) {
                    evt = (MouseEvent) events[events.length - 1];
                    doProcess(evt);
                }
            } else if (wakeup instanceof WakeupOnBehaviorPost) {
                while (true) {
                    synchronized (mouseq) {
                        if (mouseq.isEmpty()) {
                            break;
                        }
                        evt = (MouseEvent) mouseq.remove(0);
                        // consolodate MOUSE_DRAG events
                        while ((evt.getID() == MouseEvent.MOUSE_DRAGGED) &&
                                !mouseq.isEmpty() &&
                                (((MouseEvent) mouseq.get(0)).getID() ==
                                MouseEvent.MOUSE_DRAGGED)) {
                            evt = (MouseEvent) mouseq.remove(0);
                        }
                    }
                    doProcess(evt);
                }
            }

        }
        wakeupOn(mouseCriterion);
    }

    /**
     * Modified doProcess() function with transform from rotateTG
     */
    void doProcess(MouseEvent evt) {
        int id;
        int dx, dy;

        processMouseEvent(evt);

        if (((buttonPress) && ((flags & MANUAL_WAKEUP) == 0)) ||
                ((wakeUp) && ((flags & MANUAL_WAKEUP) != 0))) {
            id = evt.getID();
            if ((id == MouseEvent.MOUSE_DRAGGED) &&
                    evt.isAltDown() && !evt.isMetaDown()) {

                x = evt.getX();
                y = evt.getY();

                dx = x - x_last;
                dy = y - y_last;

                if (!reset) {
                    transformGroup.getTransform(currXform);

                    Vector3d trans = new Vector3d();
                    trans.z = dy * this.getFactor();

                    // The magic happens here:
                    Transform3D transform = new Transform3D();
                    rotateTG.getTransform(transform);
                    transform.invert();
                    transform.transform(trans);
                    // End magic

                    transformX.set(trans);

                    if (invert) {
                        currXform.mul(currXform, transformX);
                    } else {
                        currXform.mul(transformX, currXform);
                    }

                    transformGroup.setTransform(currXform);

                    transformChanged(currXform);

                //if (callback!=null)
                //callback.transformChanged( MouseBehaviorCallback.ZOOM,
                //				   currXform );

                } else {
                    reset = false;
                }

                x_last = x;
                y_last = y;
            } else if (id == MouseEvent.MOUSE_PRESSED) {
                x_last = evt.getX();
                y_last = evt.getY();
            }
        }
    }
}
