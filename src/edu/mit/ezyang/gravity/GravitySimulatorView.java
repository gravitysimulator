/*
 * GravitySimulatorView.java
 */

package edu.mit.ezyang.gravity;

import edu.mit.ezyang.gravity.j3d.utils.picking.behaviors.PickSelectCallback;
import edu.mit.ezyang.gravity.j3d.*;
import com.sun.j3d.utils.behaviors.keyboard.KeyNavigatorBehavior;
import org.jdesktop.application.Action;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import javax.swing.JDialog;

import javax.swing.JFrame;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.universe.*;
import edu.mit.ezyang.gravity.actors.*;
import java.util.*;
import javax.media.j3d.*;
import javax.swing.JPopupMenu;
import javax.vecmath.*;

/**
 * This is the main and only frame of the GravitySimulator application. It
 * controls the console and the Canvas3D, both of which respond to user
 * interaction.
 * @author Edward Z. Yang <ezyang@mit.edu>
 */
public class GravitySimulatorView extends FrameView implements PickSelectCallback {
    
    /**
     * BranchGroup that objects in the universe should be inserted in.  This
     * itself is inside a rotatable TransformGroup, which allows all objects
     * in the universe to be rotated.
     */
    public BranchGroup universeRoot;
    
    /**
     * TransformGroup corresponding to the rotation universeRoot; this
     * needs to be registered to all SmartPickTranslateBehavior objects
     * in order to compensate properly.
     */
    protected TransformGroup globalRotateGroup;
    
    /**
     * TransformGroup corresponding to the location of the viewpoint; this
     * can be modified in order to change the viewer's location.
     */
    protected TransformGroup viewTransformGroup;
    
    /**
     * Canvas being rendered in the UI. Use this to access global properties
     * including the SimpleUniverse.
     */
    protected Canvas3D canvas;
    
    /**
     * Node currently selected in the Canvas3D interface.
     */
    public Primitive selectedPrimitive;
    
    /**
     * Old material that selectedNode previously was.
     */
    protected Material selectedPrimitiveOldMaterial;
    
    /**
     * Reference to current arrow group, for rendering velocity vector.
     */
    protected BranchGroup arrowGroup;
    
    public GravitySimulatorView(SingleFrameApplication app) {
        super(app);
        
        // make menus heavy, see <http://java3d.j3d.org/faq/swing.html>
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
        initComponents();
        
        BoundingSphere bounds = new BoundingSphere(new Point3d(0f,0f,0f), 1000f);
        
        canvas = new Canvas3D(SimpleUniverse.getPreferredConfiguration());
        SimpleUniverse universe = new SimpleUniverse(canvas);
        
        BranchGroup root = new BranchGroup();
        
        // key navigation
        TransformGroup vpTrans = universe.getViewingPlatform().getViewPlatformTransform();
	KeyNavigatorBehavior keyNavBeh = new KeyNavigatorBehavior(vpTrans);
	keyNavBeh.setSchedulingBounds(bounds);
	root.addChild(keyNavBeh);
        
        universeRoot = new BranchGroup();
        universeRoot.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
        universeRoot.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
        universeRoot.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
        
        globalRotateGroup = new RotateGroup();
        
        globalRotateGroup.addChild(universeRoot);
        root.addChild(globalRotateGroup);
        universe.addBranchGraph(root);
        
        ViewingPlatform platform = universe.getViewingPlatform();
        platform.setNominalViewingTransform();
        //platform.getViewers()[0].getView().setFieldOfView(10f);
        
        viewTransformGroup = platform.getMultiTransformGroup().getTransformGroup(0);
        Transform3D viewTransform = new Transform3D();
        viewTransform.setTranslation(new Vector3f(0.0f,0.0f,10.0f));
        viewTransformGroup.setTransform(viewTransform);

        canvasPanel.add(canvas);
        
    }
    
    public void notifyPick(Primitive primitive) {
        if (selectedPrimitive != null) {
            if (selectedPrimitiveOldMaterial != null) {
                selectedPrimitive.getAppearance().setMaterial(selectedPrimitiveOldMaterial);
            }
            if (arrowGroup != null) arrowGroup.detach();
        }
        selectedPrimitive = primitive;
        Material material = selectedPrimitive.getAppearance().getMaterial();
        selectedPrimitiveOldMaterial = (Material) material.cloneNodeComponent(true);
        selectedPrimitiveOldMaterial.setCapability(Material.ALLOW_COMPONENT_WRITE);
        material.setEmissiveColor(0.5f, 0.5f, 0.5f);
        if (primitive instanceof Body) {
            // render velocity vector
            Body body = (Body) primitive;
            Node node = primitive.getParent();
            while (node != null && !(node instanceof TransformGroup)) {
                node = node.getParent();
            }
            if (node instanceof TransformGroup) {
                TransformGroup group = (TransformGroup) node;
                if (body.velocity.length() != 0f) {
                    arrowGroup = new ArrowGroup(body.velocity);
                    group.addChild(arrowGroup);
                }
            }
        }
    }
    
    /** Processes a command from the command line box, modifying Universe.
     * @param command Command line to execute.
     */
    public void processCommand(String command) {
        if (command.contains(";")) {
            for (String subcommand : command.split(";")) {
                processCommand(subcommand);
            }
            return;
        }
        
        send(command);
        String[] parts = command.split(" ");
        if (parts.length == 0 || parts[0].equals("") || parts[0].equals("help")) {
            send("Commands: add, set, view, clear");
            return;
        }
        if (parts[0].equals("add")) {
            if (parts.length == 1) {
                send("Need object to add: sphere, cone, cube or cylinder");
                return;
            }
            
            // initialize default "first class" parameters
            float scale = 0.4f;
            Vector3f location = new Vector3f(0f, 0f, 0f);
            Vector3f velocity = new Vector3f(0f, 0f, 0f);
            float mass = 10f;
            
            // initialize generic params HashMap; this can be used for
            // any sort of thing you want
            HashMap<String, String> params = new HashMap<String, String>();
            
            // parse the arguments
            if (parts.length > 2) {
                String state = null;
                for (int i = 2; i < parts.length; i++) {
                    if (state == null) {
                        state = parts[i];
                    } else {
                        if (state.equals("scale")) {
                            scale = new Float(parts[i]);
                        } else if (state.equals("at")) {
                            location = vectorize(parts[i]);
                        } else if (state.equals("velocity")) {
                            velocity = vectorize(parts[i]);
                        } else if (state.equals("mass")) {
                            mass = new Float(parts[i]);
                        } else {
                            params.put(state, parts[i]);
                        }
                        state = null;
                    }
                }
            }
            // generate the actual primitive
            Primitive primitive;
            if (parts[1].equals("body")) {
                Body body = new Body(scale);
                body.velocity = velocity;
                body.mass = mass;
                primitive = body;
            } else if (parts[1].equals("sphere")) {
                primitive = new Sphere(scale);
            } else if (parts[1].equals("cone")) {
                primitive = new Cone(scale * 1f, scale * 2f);
            } else if (parts[1].equals("cylinder")) {
                primitive = new Cylinder(scale * 1f, scale * 2f);
            } else {
                send("Unknown primitive: " + parts[1]);
                return;
            }
            primitive.getAppearance().getMaterial().setAmbientColor(.8f,.2f,.2f);
            primitive.setCapability(Primitive.ENABLE_APPEARANCE_MODIFY);
            primitive.getAppearance().setCapability(Appearance.ALLOW_MATERIAL_WRITE);
            primitive.getAppearance().getMaterial().setCapability(Material.ALLOW_COMPONENT_WRITE);
            
            primitive.setCapability(Node.ENABLE_PICK_REPORTING);
            primitive.setCapability(Node.ALLOW_BOUNDS_READ);
            primitive.setCapability(Node.ALLOW_BOUNDS_WRITE);
            
            EditableGroup group = new EditableGroup(canvas, globalRotateGroup, location, this);
            group.addTransformChild(primitive);
            
            universeRoot.addChild(group);
        } else if (parts[0].equals("view")) {
            if (parts.length == 1) {
                send("Need location parameter x,y,z");
                return;
            }
            Transform3D viewTransform = new Transform3D();
            viewTransform.setTranslation(vectorize(parts[1]));
            viewTransformGroup.setTransform(viewTransform);
        } else if (parts[0].equals("zoom")) {
            if (parts.length == 1) {
                send("Need zoom value z (view 0,0,z)");
                return;
            }
            Transform3D viewTransform = new Transform3D();
            viewTransform.setTranslation(vectorize("0,0," + parts[1]));
            viewTransformGroup.setTransform(viewTransform);
        } else if (parts[0].equals("set")) {
            if (selectedPrimitive == null) {
                send("No primitive selected");
                return;
            }
            if (!(selectedPrimitive instanceof Body)) {
                send("Cannot set parameters for non-Body object");
                return;
            }
            Body primitive = (Body) selectedPrimitive;
            if (parts.length == 1) {
                send("Need variable parameter: velocity, mass");
                return;
            }
            if (parts[1].equals("velocity")) {
                if (parts.length == 2) {
                    send("Need velocity vector x,y,z");
                    return;
                }
                primitive.velocity = vectorize(parts[2]);
                notifyPick(primitive);
            } else if (parts[1].equals("mass")) {
                if (parts.length == 2) {
                    send("Need mass value m");
                    return;
                }
                primitive.mass = new Float(parts[2]);
            } else {
                send("Unknown variable " + parts[1]);
            }
        } else if (parts[0].equals("clear")) {
            universeRoot.removeAllChildren();
        } else {
            send("Unknown command: " + parts[0] + ", type help for options");
            return;
        }
    }
    
    /** Converts string in form x,y,z to Vector3f
     * @param input vector in form x,y,z
     * @return Appropriate vector
     */
    protected Vector3f vectorize(String input) {
        String[] vectorFields = input.split(",");
        return new Vector3f(new Float(vectorFields[0]), new Float(vectorFields[1]), new Float(vectorFields[2]));
    }
    
    /** Sends a message to display in the terminal.
     * @param message Message to send
     */
    protected void send(String message) {
        terminal.append(message + "\n");
    }

    /** Displays the about box.
     */
    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = GravitySimulatorApp.getApplication().getMainFrame();
            aboutBox = new GravitySimulatorAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        GravitySimulatorApp.getApplication().show(aboutBox);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        canvasPanel = new javax.swing.JPanel();
        console = new javax.swing.JPanel();
        commandLine = new javax.swing.JTextField();
        terminalPane = new javax.swing.JScrollPane();
        terminal = new javax.swing.JTextArea();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();

        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setLayout(new java.awt.BorderLayout());

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(edu.mit.ezyang.gravity.GravitySimulatorApp.class).getContext().getResourceMap(GravitySimulatorView.class);
        canvasPanel.setBackground(resourceMap.getColor("canvasPanel.background")); // NOI18N
        canvasPanel.setName("canvasPanel"); // NOI18N
        canvasPanel.setLayout(new java.awt.BorderLayout());
        mainPanel.add(canvasPanel, java.awt.BorderLayout.CENTER);

        console.setName("console"); // NOI18N
        console.setLayout(new java.awt.BorderLayout());

        commandLine.setText(resourceMap.getString("commandLine.text")); // NOI18N
        commandLine.setName("commandLine"); // NOI18N
        commandLine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                commandLineActionPerformed(evt);
            }
        });
        console.add(commandLine, java.awt.BorderLayout.CENTER);

        terminalPane.setName("terminalPane"); // NOI18N

        terminal.setColumns(20);
        terminal.setEditable(false);
        terminal.setFont(resourceMap.getFont("terminal.font")); // NOI18N
        terminal.setRows(5);
        terminal.setName("terminal"); // NOI18N
        terminalPane.setViewportView(terminal);

        console.add(terminalPane, java.awt.BorderLayout.PAGE_START);

        mainPanel.add(console, java.awt.BorderLayout.PAGE_END);

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(edu.mit.ezyang.gravity.GravitySimulatorApp.class).getContext().getActionMap(GravitySimulatorView.class, this);
        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setComponent(mainPanel);
        setMenuBar(menuBar);
    }// </editor-fold>//GEN-END:initComponents

private void commandLineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_commandLineActionPerformed
    processCommand(commandLine.getText());
    commandLine.setText("");
}//GEN-LAST:event_commandLineActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel canvasPanel;
    private javax.swing.JTextField commandLine;
    private javax.swing.JPanel console;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JTextArea terminal;
    private javax.swing.JScrollPane terminalPane;
    // End of variables declaration//GEN-END:variables

    private JDialog aboutBox;
}
